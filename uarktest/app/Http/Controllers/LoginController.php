<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function showLoginPage()
    {
        return View('login');
    }

    public function login(Request $request)
    {

        $userData = User::where('account', $request->account)->first();

        if(!isset($userData->account)) {
            return view('login', ['err'=>"帳號不存在"]);
        } elseif (Hash::check($request->password, $userData->password) && $userData->status = '待審') {
            session(['account' => $userData->account]);

            return view('/index', [
                'account' => $userData->account,
                'message' => "此帳號待審核開通!"
            ]);

        } elseif (Hash::check($request->password, $userData->password)) {
            session(['account' => $userData->account]);
            return view('/index');
        } else {
            return view('login', ['err'=>"密碼錯誤"]);
        }
    }

    public function logout()
    {
        session()->forget('account');
        return redirect('/login');
    }
}
