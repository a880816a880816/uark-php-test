<?php

namespace Database\Factories;

use App\Models\Org;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Log;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        Log::info("empty user");
        $org = Org::first();
        return [
            'org_id' => $org->id,
            'name' => $this->faker->name(),
            'birthday' => '1999/08/16',
            'email' => $this->faker->unique()->safeEmail(),
            'account' => 'admin',
            'password' => '1234', // password
            'status' => '待審',
        ];
        
    }
}
