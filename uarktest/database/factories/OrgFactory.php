<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrgFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
                'title' => '放射科',
                'org_no' => '001',
            ];
        
    }
}
