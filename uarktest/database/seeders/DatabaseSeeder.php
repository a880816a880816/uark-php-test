<?php

namespace Database\Seeders;

use App\Models\Org;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $orgs = Org::all();
        if ($orgs) {
            \App\Models\Org::factory(1)->create();
        }else {
            Log::info($orgs);
        }

        $users = User::all();
        if ($users) {
            \App\Models\User::factory(1)->create();
        }

    }
}
